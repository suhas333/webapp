# -*- coding: utf-8 -*-
"""Setup the webapp application"""
from __future__ import print_function
from webapp import model


def bootstrap(command, conf, vars):
    """Place any commands to setup webapp here"""

    # <websetup.bootstrap.before.auth

    # <websetup.bootstrap.after.auth>
